# -*- coding: utf-8 -*-
import re
import json
import scrapy
from urllib.parse import unquote
from scrapy.shell import inspect_response


class CpusSpider(scrapy.Spider):
    name = 'cpus'
    allowed_domains = ['psref.lenovo.com']
    start_urls = ['https://psref.lenovo.com/']

    def parse(self, response):
        items_css = '#dialog_main #Laptops a ::attr(href), #dialog_main #Workstations a ::attr(href)'
        items = [re.sub('\?.*', '', response.urljoin(url)) for url in response.css(items_css).extract() if url.startswith('/Product/')]
        items = sorted(list(set(items)))

        for item in items:
            yield response.follow(item, callback=self.find_specs_page)

    def find_specs_page(self, response):
        # inspect_response(response, self)

        d = {}
        d['product'] = _clear_and_join(response.css('h1::text').extract())
        d['product_url'] = response.url
        d['specs_url'] = response.css('#divSpec iframe#ifr_SpecPage::attr(src)').extract_first()
        d['pdf_url'] = response.css('#divSpec .pdfdiv object::attr(data)').extract_first()

        d['specs_url'] = response.urljoin(d['specs_url']) if d['specs_url'] else None
        d['pdf_url'] = response.urljoin(d['pdf_url']) if d['pdf_url'] else None

        if d['specs_url']:
            return response.follow(d['specs_url'], meta={'item': d}, callback=self.parse_specs_page)
        else:
            return d

    def parse_specs_page(self, response):
        # inspect_response(response, self)

        d = response.meta['item']
        d['specs'] = parse_specs(response)

        return d


def parse_specs(response):
    trs_css = '[specstructure="Processor"] .divFeatureValue .fn-content-table tr'
    trs = response.css(trs_css)

    header = [td for td in trs[0].css('td::text').extract()]
    body = [[_clear_and_join(td.css('td::text').extract()) for td in tr.css('td')] for tr in trs[1:]]

    specs = {
        'processor': [dict(zip(header,row)) for row in body]
    }

    return specs


def _clear_and_join(lst, sep=' '):
    lst = [re.sub('[\xa0\r\t ]+', ' ', t, flags=re.DOTALL).strip() for t in lst]
    lst = list(filter(None, [re.sub('\s*\n\s*', sep, t, flags=re.DOTALL).strip() for t in lst]))

    text = sep.join(lst)

    return text
